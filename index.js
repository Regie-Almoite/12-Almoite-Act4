console.log("Hello World!");

let userInformation = {
    firstName: "John",
    lastName: "Smith",
    age: 30,
    hobbies: ["Biking", "Mountain Climbing", "Swimming"],
};

const workAddress = "";
const city = "Lincoln";
const houseNumber = 32;
const state = "Nebraska";
const street = "Washington";
const isMarried = true;

// userInformation function
function displayUserInfo(userInfo) {
    console.log(
        `${userInfo.firstName} ${userInfo.lastName} is ${userInfo.age} years of age`
    );
}
displayUserInfo(userInformation);

// hobbies function
function displayHobbies(userinfo) {
    console.log(`His hobbies are: ${userinfo.hobbies}`);
}
displayHobbies(userInformation);

console.log(`city: ${city}`);
console.log(`house Number: ${houseNumber}`);
console.log(`state: ${state}`);
console.log(`street: ${street}`);

// Relationship status function
function displayRelationsipStatus(isMarried) {
    console.log("The value of isMarried is:");
    console.log(isMarried);
}

displayRelationsipStatus(isMarried);
